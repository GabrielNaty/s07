/*Learning Objectives:
	- create code that will execute only when a given condition has been made
Topics:
	- Assignment Operators
	- Conditional Statements
	- Ternary Operator

// Lesson Proper
Conditional Statements
	- a conditional statement is one of the key features of a programming language

	Ex.
		is the container full or not?
		is the temperature higher than or equal to 40 degrees celsius?
		does the title contain an ampersand(&) character?

There are three types of conditional statements:
	- if-else statements
	- switch statements
	- try-catch-finally statements

Operators
	- allow programming languages to execute operations or evaluations

	Assignment Operators
	- assign a value to a variable
*/
// Basic Assignment Operator (=)
	// it allows us to assign a value to a variable
	let variable = "initial value";

// Mathematical Operators (addition (+), subtraction (-), multiplication (*), division (/), modulo (%))
// Whenever you have used a mathematical operator, a value is returned, it is only up to us if we save that returned value.
// Addition, subtraction, multiplication, division assignment operators allows us to assign the result of the operation to the value of the left operand. It allows us to save the result of a mathematical operation to the left operand.

	let num1 = 5;
	let num2 = 10;
	let num3 = 4; 
	let num4 = 40;

	/*
		Addition Assignment Operator (+=):
			Left operand
				- is the variable or value of the left side of the operator
			Right operand
				- is the variable or value of the right side of the operator
			ex. sum = num1 + num 4 > re-assigned the value of sum with the result of num1 + num4
	*/

	let sum1 = num1 + num4;

	//num1 = num1 + num4;
	num1 += num4;
	console.log(num1);

	num2 += num3;
	console.log(num2);

	num1 += 55;
	console.log(num1);
	console.log(num4); //40 - previous right should not be affected/ re-assigned

	let string1 = "Boston";
	let string2 = "Celtics";
	// string1 = string1 + string2
	string1 += string2;
	console.log(string1); //BostonCeltics - results in concatenation between 2 strings and saves the resuts in the left operand
	console.log(string2); //Celtics 

	// 15 += num1; it produces error, because we do not use assignment operator when the left operand is just a value/data

	// subtraction assignment operator (-=)
	num1 -= num2;
	console.log(num1);
	console.log("The result of subtraction assignment operator: " + num1);

	// multiplication assigment operator (*=)
	num2 *= num3;
	console.log("Result of multiplication assignment operator: " + num2);

	//division multiplication assignment operator (/=)
	num4 /= num3;
	console.log("Result of division assignment operator: " + num4);


// [SECTION] Arithmetic Operators
let x = 1397;
let y = 7831;

let sum2 = x + y;
console.log("Result of addition operator: " + sum2);

let difference = y - x;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulus operator: " + remainder); 

// Multiple Operators and Parenthesis
/*
	- when multiple operators are applied in a single statement, it follows the PEMDAS rule (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	- the operators were done in the following
		expression = 1 + 2 - 3 * 4 / 5;
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of multiple operations: " + mdas);

// order of operation can be changed by adding parentheses to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
/*
	- by adding parentheses "()", the order of operation are changed prioritizing operations inside the parentheses first then follow mdas rule
	- the operations were done in the following order:
*/
console.log("The result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation 2nd example: " + pemdas);

// Increment and Decrement
	// Increment and decrement is adding or subtracting 1 from the variable and re-assignning the new value to the variable where the increment or decrement was used.
	// 2 Kinds of Incrementation: Pre-fix and Post-fix

	let z = 1;

	// Pre-fix Incrementation
	++z;
	console.log("Pre-fix increment: " + z);

	// Post-fix Incrementation
	z++;
	console.log("Post-fix increment: " + z);//3 - The value of z was added with 1
	console.log(z++);//3 - with post - incrementation the previous value of the variable is returned first before the actual incrementation
	console.log(z);//4 - new value is now returned

	// Pre-fix vs Post-fix
	console.log(z);
	console.log(z++);//4 previous value was returned first
	console.log(z);//5 new value is now returned

	console.log(++z);

	// Pre-fix and Post-fix Decrementation
	console.log(--z);
	console.log(z--);
	console.log(z);

// Comparison Operators
	/*Comparison Operators 
		- are used to compare the values of the left and right operands
		- comparison operators return boolean
			- Equality or Loose equality operator (==)
			- Strict equality operator (===)

	*/
	console.log(1 == 1);//true

	let isSame = 55 == 55;
	console.log(isSame);

	console.log(1 == '1');//true - loose equality operator priority is the sameness of the value because with loose equality operator, forced coercion is done before comparison - JS forcibly changes the data type to the operands

	console.log(0 == false);//true - with forced coercion, false was converted into a number results into NaN so therefore is false, 1 is not equal to NaN
	console.log(1 == true);//true
	console.log("false" == false);//false
	console.log(true == "true");//false
	/*
		with loose comparison operator (==), values are compared and types if operands do not have the same types, it will be forced coerced/type coerce before comparison value

		if either the operand is a number of boolean, the operands are converted into numbers
	*/
	console.log(true == "1");//true - true coerced into a number = 1, "1" converted into a number = 1 - 1 equals 1

	// Strict Equality (===)
	console.log(true === "1");//false

	//check both value and type
	console.log(1 === '1');//false - operands have the same valued by different types 
	console.log("BTS" === "BTS");//true - same value and same typed
	console.log("Marie" === "marie");//false - left operand is capitalized and right operand is small caps

	// Inequality Operators (!=)
		/* Loose inequality operators
			- checks whether the operands are NOT equal or have different values
			- will do type coercion if the operands have different			
		*/
	console.log('1' != 1);//false
	/*
		false = both operands were converted to numbers
			'1' converted to number is 1
			1 converted into number is 1 - 1 equals 1 not inequal
	*/
	console.log("James" != "John");
	console.log(1 != true);//false
	/*
		false - with type conversion: true was converted to 1
		1 is equal to 1
		it is NOT inequal
	*/

	// Strict inequality (!==) - checks whether the two operands have different values and will check if they have different types
	console.log("S" !== 5);//true - operands are inequal because they have different types
	console.log(5 != 5);//false - operands are equal they have the same value and they have the same type

	let name1 = "Jin";
	let name2 = "Jimin";
	let name3 = "Jungkook";
	let name4 = "V";

	let number1 = 50;
	let number2 = 60;
	let numString1 = "50";
	let numString2 = "60";

	console.log(numString1 == number1);//true
	console.log(numString1 === number1);//false
	console.log(numString1 != number1);//false
	console.log(name4 !== "num3");//true
	console.log(name3 == "Jungkook");//true
	console.log(name1 === "Jin");//true

/* Relational comparison operators
	a comparison operator compares its operand and returns a boolean value based on whether the comparison is true
*/

	let a = 500;
	let b = 700;
	let c = 8000;
	let numString3 = "5500";

	// Greater than (>)
	console.log(a > b);
	console.log(c > y);

	// Less than (<)
	console.log(c < a);//false
	console.log(b < b);//false
	console.log(a < 1000);//true
	console.log(numString3 < 1000);//false - forced coercion to change the string into a number
	console.log(numString3 < 6000);//true
	console.log(numString3 < "Jose");//true - "5000" < "Jose" - that is erratic (unpredictable)

	// Greater than or equal to (>=)
	console.log(c >= 10000);//false
	console.log(b >= a);//true

	// Less than or equal to (<=)
	console.log(a <= b);//true
	console.log(c <= a);//false
/* Logical Operators
	AND operator (&&)
		- both operands on the left and right or all operands must be true otherwise it is false

		T && T = true
		T && F = false
		F && T = false
		F && F = false
*/

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1);//false

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2);//true

	let requiredLevel = 95;
	let requiredAge = 18;

	let authorization3 = isRegistered && requiredLevel === 25; 
	console.log(authorization3);//false

	let authorization4 = isRegistered && isLegalAge && requiredLevel == 95
	console.log(authorization4);//true

	let userName = "gamer2001";
	let userName2 = "shadow1991";
	let userAge = 15;
	let userAge2 = 30;

	let registration1= userName.length > 8 && userAge >= requiredAge;
	// .length is a property of string which determine the number of characters in the string.
	console.log(registration1)

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2)

	/* OR Operator (|| - double pipe)
		- returns true if at least one of the operands are true.
		T || T = true
		T || F = true
		F || T = true
		F || F = false */

	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirement1);//false

	let guildRequirement2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement2);//true

	let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement3);//true

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin);//false

	//Not Operator - it turns a boolean into the opposite value


	let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin2);//true

	console.log(!isRegistered);

//[SECTION] Conditional
	//if-else statements - will run a block of code if the condition specified is true or results to true

	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	// if(true){
	// 	alert("We just run an if condition!")
	// };

	if(userName3.length	> 10){
		console.log("Welcome to Game Online!")
	};

	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the guild.")
	};

	// else statement will be run if the condition given is false or results to false

	if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log("Thank you for joining the Noobies Guild");
	} else {
		console.log("You are too strong to be a noob. :(");
	};

	// else if - executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true
	if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge) {
		console.log("Thank you for joining the Noobies Guild");
	} else if(userLevel3 > 25){
		console.log("You are too strong to be a noob.");
	} else if(userAge3 < requiredAge) {
		console.log("You are too young to join the guild.");
	} else if(userName3.length < 10) {
		console.log("Username too short.");
	};

	// if-else in function
	function addNum(num1, num2) {
		// check if the numbers being passed argument are number types
		// typeof keyword returns a string which tells the type of data that follow it
		if(typeof num1 === 'number' && typeof num2 === 'number'){
			console.log("run only if both arguments passed are number types")
			console.log(num1 + num2);
		} else {
			console.log("One or both of the arguments are not numbers");
		};
	};

	addNum(5, "2");

/*	function login(username, password){
		// how can we check if the argument passed are strings?
		if(typeof username === 'string' && typeof password === 'string') {
			console.log("Both arguments are strings.");
			 Nested if-else
				will run if the parent if statement is able to agree to accomplish its condition

				Mini-Activity
					- add another condition to our nested if statement:
						check if the password is at least 8 characters long
					- add an else statement which will run if both conditions are not met:
						- show an alert which says "Credentials too short."

					Stretch Goals:
						- add an else if statement that if the username is less than 8 characters
							- show an alert "username is too short."
						- add an else if statement that if the password is less than 8 characters
							- show an alert "password is too short."
			
			if(password.length >= 8) {			
				console.log("Successfully Logged-in");
			} else if(username.length < 8) {
				alert("username is too short.");	
			} else if(password.length < 8) {
				alert("password is too short.");
			} else {
				alert("Credentials too short.");
			};				
		};
	};

login("12345678", "12345678");
*/

/*
	Mini-Activity
		Create a function which will determine the color shirt to wear depending on the day the user inputs
			Monday - Black
			Tuesday - Green
			Wednesday - Yellow
			Thursday - Red
			Friday - Violet
			Saturday - Blue
			Sunday - White
		Log a message on alert window:
			"Today is <dayToday>, Wear <colorOfTheDay>"

		If the day input is out of range, log message on the alert window:
			"Invalied input. Enter a valied day of the week."
		
		stretch Goals:
		Check if the argument passed is string:
			Log an alert if it is not:
				"Invalid Input. Please input a string."
			Research and use .toLowerCase(), so user can input in lowecase.

			If you'r done, send screenshot of your browser showing your sonsole and your used of the function in our SB discussion - Mini-Activity: s07a - Selection Structure

*/

// Solution for Mini-Activity
/*function colorOfTheDay(day) {
	if(typeof day === "string"){
		// .toLowerCase is used on string, it forcibly changes the case of the string to lowercase
		if(day.toLowerCase() === "monday") {
			alert(`Today is ${day}. Wear Black.`);
		} else if(day.toLowerCase() === "tuesday") {
			alert(`Today is ${day}. Wear Green.`);
		} else if(day.toLowerCase() === "tuesday") {
			alert(`Today is ${day}. Wear Green.`);
		} else if(day.toLowerCase() === "wednesday") {
			alert(`Today is ${day}. Wear Yellow.`);
		} else if(day.toLowerCase() === "thursday") {
			alert(`Today is ${day}. Wear Red.`);
		} else if(day.toLowerCase() === "friday") {
			alert(`Today is ${day}. Wear Violet.`);
		} else if(day.toLowerCase() === "saturday") {
			alert(`Today is ${day}. Wear Blue.`);
		} else if(day.toLowerCase() === "sunday") {
			alert(`Today is ${day}. Wear White.`);
		} else {
			alert("Invalid Input. Enter a valid day of the week.")
		}
	}
}*/



// My Solution
/*function determineDayShirtColor(inputday) {
	if(typeof inputday === 'string') {
		let lowinputday = inputday.toLowerCase();
		if(lowinputday == "monday") {
			alert("Today is Monday, Wear Black");
		} else if(lowinputday == "tuesday") {
			alert("Today is Tuesday, Wear Green");
		} else if(lowinputday == "wednesday") {
			alert("Today is Wednesday, Wear Yellow");
		} else if(lowinputday == "thursday") {
			alert("Today is Thursday, Wear Red");
		} else if(lowinputday == "friday") {
			alert("Today is Friday, Wear Violet");
		} else if(lowinputday == "saturday") {
			alert("Today is Saturday, Wear Blue");
		} else if(lowinputday == "friday") {
			alert("Today is Sunday, Wear White");
		} else {
			alert("Invalid input. Enter a valid day of the week.");
		};			
	} else {
		alert("Invalid Input. Please input a string.");
	};
};

determineDayShirtColor("FRIDAY");*/

/*Switch Statement
	- is used an alternative to an if, else-if or else tree, where the data being evaluated or checked in an expected input
	- if we want to select one of many code blocks/ statements to be executed.
	syntax:
		switch(expression) {
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/
let hero = "Anpanman"

switch(hero) {
	case "Jose Rizal":
		console.log("Philippines National Hero");
		break;
	case "George Washington":
		console.log("Hero of the American Revolution");
		break;
	case "Hercules":
		console.log("Legendary Hero of the Greek");
		break;
	case "Anpanman":
		console.log("Superhero!");
		break;	
};

function roleChecker(role) {
	switch(role) {
		case "Admin":
			console.log("Welcome Admin, to the Dashboard");
			break;
		case "User":
			console.log("You are not authorized to view this page");
			break;
		case "Guest":
			console.log("Go to the registration page to register.");
			break;
			// break it breaks/terminates the code block. If this was not added to your case then, the next case will run as well.
		default:
		// by default your switch ends with default case, so therefore, even if there is no break keyword in your default case, it will not run anything else
			console.log("Invalid Role");
	};
};

roleChecker("Admin");

/*
	Mini-Activity
		create a colorOfTheDay function, instead of using if-else, convert it to a switch.
*/

function colorOfTheDay(inputday) {
	let day = inputday.toLowerCase();
	if(typeof day === "string") {
		switch(day) {
			case "monday":
				alert(`Today is ${day}. Wear Black.`);
				break;
			case "tuesday":
				alert(`Today is ${day}. Wear Green.`);
				break;
			case "wednesday":
				alert(`Today is ${day}. Wear Yellow.`);
				break;
			case "thursday":
				alert(`Today is ${day}. Wear Red.`);
				break;
			case "friday":
				alert(`Today is ${day}. Wear Violet.`);
				break;
			case "saturday":
				alert(`Today is ${day}. Wear Blue.`);
				break;
			case "sunday":
				alert(`Today is ${day}. Wear White.`);
				break;
			default:
				alert("Invalid Input. Enter a valid day of the week.");
		};
	} else {
		alert("Invalid Input. Please input a string.");
	}
};

colorOfTheDay("FRIDAY");

//function with if-else and return
function gradeEvaluator(grade) {
	/*
		evaluate the grade input and reutrn the letter distinction
			- if the grade is less than or equal to 70 = F
			- if the grade is greater than or equal to 71 = C
			- if the grade is greater than or equal to 80 = B
			- if the grade is greater than or equal to 90 = A
	*/

	if(grade >= 90) {
		return "A";
	} else if(grade >= 80) {
		return "B";
	} else if(grade >= 71) {
		return "C";
	} else if(grade <= 70) {
		return "F";
	} else {
		return "Invalid"
	}

	/* My Solution

	let outputgrade = null;
	if(grade <= 70) {
		outputgrade = "F";
	} else if (grade <= 79) {
		outputgrade = "C";
	} else if (grade <= 89) {
		outputgrade = "B";
	} else if (grade <= 100) {
		outputgrade = "A";
	}
	return outputgrade;*/
};	
	
let grade = gradeEvaluator(75);
console.log(`The result is ${grade}`);

/* Ternary Operators */
	// shorthand way of writing if-else statements
	/*
		syntax:
		condition ? if-statement : else statement
	*/

let price = 5000; 

price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000")

let hero1 = "Goku";

hero1 === "Vegeta" ? console.log("You are the Prince of all Saiyans.") : console.log("You are not even royalty.")	

let villain = "Harvey Dent";

villain === "Two Faces"
? console.log("You live long enough to be a villain.")
: console.log("Not quite villainous yet.")

let robin = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin 
? true
: false;
console.log(isFirstRobin);

let number = 7;

//ternary operator is not meant for complex
/*number === 5
? console.log("A")
: (number === 10) console.log("A is 10") : console.log("A is not 5 or 10));*/


