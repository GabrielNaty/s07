function oddEvenChecker(inputnum) {
	if(typeof inputnum === "number") {
		rem = inputnum % 2;
		rem == 0
		? console.log("The number is even")
		: console.log("The number is odd");
	} else {
		alert("Invalid Input.");
	};
};

oddEvenChecker(2);
oddEvenChecker(1);
oddEvenChecker("3");

function budgetChecker(inputnum) {
	if(typeof inputnum === "number") {		
		inputnum > 40000
		? console.log("You are over the budget")
		: console.log("You have resources left");
	} else {
		alert("Invalid Input.");
	};	
}

budgetChecker(40001);
budgetChecker(40000);
budgetChecker("1234");


